from rest_framework import status
from rest_framework.test import APITestCase


class FizzBuzzTests(APITestCase):
    fixtures = ['fixtures/test_fixtures.json']

    def setUp(self):
        data = {"username": "test", "password": "test123"}
        response = self.client.post('/api-token-auth/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.access_token = response.data['token']
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.access_token)

    def tearDown(self):
        pass

    def test_fizz_buzz_algorithm(self):
        data = {"amount_of_result": 50, "offset": 1, "result_format": 'json'}

        response = self.client.post('/api/7276D1C1EF24/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {1: 1, 2: 2, 3: 'Fizz', 4: 4, 5: 'Buzz', 6: 'Fizz', 7: 7, 8: 8,
                                         9: 'Fizz', 10: 'Buzz', 11: 11, 12: 'Fizz', 13: 13, 14: 14, 15: 'FizzBuzz',
                                         16: 16, 17: 17, 18: 'Fizz', 19: 19, 20: 'Buzz', 21: 'Fizz', 22: 22,
                                         23: 23, 24: 'Fizz', 25: 'Buzz', 26: 26, 27: 'Fizz', 28: 28, 29: 29,
                                         30: 'FizzBuzz', 31: 31, 32: 32, 33: 'Fizz', 34: 34, 35: 'Buzz',
                                         36: 'Fizz', 37: 37, 38: 38, 39: 'Fizz', 40: 'Buzz', 41: 41,
                                         42: 'Fizz', 43: 43, 44: 44, 45: 'FizzBuzz', 46: 46, 47: 47, 48: 'Fizz',
                                         49: 49})

    def test_result_format_html(self):
        data = {"amount_of_result": 50, "offset": 1, "result_format": 'html'}

        response = self.client.post('/api/7276D1C1EF24/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, "<html><body><table><thead><th>n</th><th>string</th><thead><tbody><tr>"
                                        "<td>1</td><td>1</td></tr><tr><td>2</td><td>2</td></tr><tr><td>3</td>"
                                        "<td>Fizz</td></tr><tr><td>4</td><td>4</td></tr><tr><td>5</td><td>Buzz</td>"
                                        "</tr><tr><td>6</td><td>Fizz</td></tr><tr><td>7</td><td>7</td></tr><tr>"
                                        "<td>8</td><td>8</td></tr><tr><td>9</td><td>Fizz</td></tr><tr><td>10</td>"
                                        "<td>Buzz</td></tr><tr><td>11</td><td>11</td></tr><tr><td>12</td><td>Fizz</td>"
                                        "</tr><tr><td>13</td><td>13</td></tr><tr><td>14</td><td>14</td></tr><tr><td>15"
                                        "</td><td>FizzBuzz</td></tr><tr><td>16</td><td>16</td></tr><tr><td>17</td>"
                                        "<td>17</td></tr><tr><td>18</td><td>Fizz</td></tr><tr><td>19</td><td>19</td>"
                                        "</tr><tr><td>20</td><td>Buzz</td></tr><tr><td>21</td><td>Fizz</td></tr><tr>"
                                        "<td>22</td><td>22</td></tr><tr><td>23</td><td>23</td></tr><tr><td>24</td>"
                                        "<td>Fizz</td></tr><tr><td>25</td><td>Buzz</td></tr><tr><td>26</td><td>26</td>"
                                        "</tr><tr><td>27</td><td>Fizz</td></tr><tr><td>28</td><td>28</td></tr><tr>"
                                        "<td>29</td><td>29</td></tr><tr><td>30</td><td>FizzBuzz</td></tr><tr>"
                                        "<td>31</td><td>31</td></tr><tr><td>32</td><td>32</td></tr><tr><td>33</td>"
                                        "<td>Fizz</td></tr><tr><td>34</td><td>34</td></tr><tr><td>35</td><td>Buzz</td>"
                                        "</tr><tr><td>36</td><td>Fizz</td></tr><tr><td>37</td><td>37</td></tr><tr>"
                                        "<td>38</td><td>38</td></tr><tr><td>39</td><td>Fizz</td></tr><tr><td>40</td>"
                                        "<td>Buzz</td></tr><tr><td>41</td><td>41</td></tr><tr><td>42</td><td>Fizz</td>"
                                        "</tr><tr><td>43</td><td>43</td></tr><tr><td>44</td><td>44</td></tr><tr>"
                                        "<td>45</td><td>FizzBuzz</td></tr><tr><td>46</td><td>46</td></tr><tr>"
                                        "<td>47</td><td>47</td></tr><tr><td>48</td><td>Fizz</td></tr><tr><td>49</td>"
                                        "<td>49</td></tr></tbody></table></body></html>")

    def test_result_format_csv(self):
        data = {"amount_of_result": 50, "offset": 1, "result_format": 'csv'}

        response = self.client.post('/api/7276D1C1EF24/', data, format='json')
        self.assertEqual.__self__.maxDiff = None
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, "'n','string'\r\n'1','1'\r\n'2','2'\r\n'3','Fizz'\r\n'4','4'\r\n'5',"
                                        "'Buzz'\r\n'6','Fizz'\r\n'7','7'\r\n'8','8'\r\n'9','Fizz'\r\n'10',"
                                        "'Buzz'\r\n'11','11'\r\n'12','Fizz'\r\n'13','13'\r\n'14','14'\r\n'15',"
                                        "'FizzBuzz'\r\n'16','16'\r\n'17','17'\r\n'18','Fizz'\r\n'19','19'\r\n'20',"
                                        "'Buzz'\r\n'21','Fizz'\r\n'22','22'\r\n'23','23'\r\n'24','Fizz'\r\n'25',"
                                        "'Buzz'\r\n'26','26'\r\n'27','Fizz'\r\n'28','28'\r\n'29','29'\r\n'30',"
                                        "'FizzBuzz'\r\n'31','31'\r\n'32','32'\r\n'33','Fizz'\r\n'34','34'\r\n'35',"
                                        "'Buzz'\r\n'36','Fizz'\r\n'37','37'\r\n'38','38'\r\n'39','Fizz'\r\n'40',"
                                        "'Buzz'\r\n'41','41'\r\n'42','Fizz'\r\n'43','43'\r\n'44','44'\r\n'45',"
                                        "'FizzBuzz'\r\n'46','46'\r\n'47','47'\r\n'48','Fizz'\r\n'49','49'\r\n")

    def test_result_format_plain(self):
        data = {"amount_of_result": 50, "offset": 1, "result_format": 'plain'}

        response = self.client.post('/api/7276D1C1EF24/', data, format='json')
        self.assertEqual.__self__.maxDiff = None
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, "n	string\n1	1\n2	2\n3	Fizz\n4	4\n5	Buzz\n6	Fizz\n7	7\n8	8"
                                        "\n9	Fizz\n10	Buzz\n11	11\n12	Fizz\n13	13\n14	14\n15	FizzBuzz\n"
                                        "16	16\n17	17\n18	Fizz\n19	19\n20	Buzz\n21	Fizz\n22	22\n23	23\n"
                                        "24	Fizz\n25	Buzz\n26	26\n27	Fizz\n28	28\n29	29\n30	FizzBuzz\n"
                                        "31	31\n32	32\n33	Fizz\n34	34\n35	Buzz\n36	Fizz\n37	37\n38	38\n"
                                        "39	Fizz\n40	Buzz\n41	41\n42	Fizz\n43	43\n44	44\n45	FizzBuzz\n"
                                        "46	46\n47	47\n48	Fizz\n49	49\n")


class MustFailTests(APITestCase):
    fixtures = ['fixtures/test_fixtures.json']

    def setUp(self):
        data = {"username": "test2", "password": "TestSocios123"}
        response = self.client.post('/api-token-auth/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.access_token = response.data['token']
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.access_token)

    def tearDown(self):
        pass

    def test_fizz_buzz_algorithm(self):
        data = {"amount_of_result": 100, "result_format": 'json'}

        response = self.client.post('/api/84E9ECE5A60C/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, "You don't have enough permission!")