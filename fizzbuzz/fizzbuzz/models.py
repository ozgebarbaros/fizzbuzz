import uuid
from django.db import models
from django.contrib.auth.models import Group


class Divisor(models.Model):
    string = models.CharField(verbose_name="String", max_length=100, blank=False, null=False)
    number = models.IntegerField(verbose_name="Number", blank=False, null=False)

    def __str__(self):
        return "{0}/{1}".format(self.string, self.number)


class Generator(models.Model):
    name = models.CharField(verbose_name="Name", max_length=100)
    divisor = models.ManyToManyField(Divisor, related_name='divisor')
    is_published = models.BooleanField(default=False)
    url = models.URLField(verbose_name="URL", unique=True)
    permited_groups = models.ManyToManyField(Group)

    def __str__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None, *args, **kwargs):
        try:
            Generator.objects.get(pk=self.pk)
        except:
            unique_url = uuid.uuid4().hex[:12].upper()
            self.url = unique_url
        super(Generator, self).save(force_insert, force_update, *args, **kwargs)



