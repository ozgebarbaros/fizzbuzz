def check_if_dividable(num, divisor):
    return True if num % divisor == 0 else False


def custom_fizz_bizz(generator, amount_of_result, offset):
    results = {}
    all_divisors = generator.divisor.all()

    if offset < amount_of_result:
        for num in range(offset, amount_of_result):
            concated_str = ''
            for div in all_divisors:
                concated_str += div.string if check_if_dividable(int(num), div.number) else ''
            if not concated_str:
                concated_str = num
            results[num] = concated_str
    else:
        results['message'] = "Inconvenient range of integer"

    return results


def dict_to_csv(result):
    csv_formated = "'n','string'\r\n"
    for item in result.items():
        csv_formated += "'{0}','{1}'\r\n".format(item[0], item[1])
    return csv_formated


def dict_to_html(result):
    html_formatted = "<html><body><table><thead><th>n</th><th>string</th><thead><tbody>"
    for item in result.items():
        html_formatted += "<tr><td>{0}</td><td>{1}</td></tr>".format(item[0], item[1])
    return html_formatted + "</tbody></table></body></html>"


def dict_to_plain(result):
    plain_formated = "n\tstring\n"
    for item in result.items():
        plain_formated += "{0}\t{1}\n".format(item[0], item[1])
    return plain_formated


def get_result_by_format(result, format):
    return {
        'json': result,
        'csv': dict_to_csv(result),
        'html': dict_to_html(result),
        'plain': dict_to_plain(result)
    }.get(format)


def has_permission(user, generator):
    for group in generator.permited_groups.all():
        if user.groups.filter(name=group.name).exists():
            return True
    return False
