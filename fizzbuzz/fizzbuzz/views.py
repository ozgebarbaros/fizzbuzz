from django.http import HttpResponse
from rest_framework import response, status
from rest_framework.decorators import api_view
from .models import Generator
from .utils import custom_fizz_bizz, get_result_by_format, has_permission


@api_view(['POST', 'GET'])
def call_generator(request, url=None):
    generator = Generator.objects.get(url=url)
    if has_permission(request.user, generator):
        amount_of_result = request.data.get('amount_of_result', 50)
        offset = request.data.get('offset', 1)
        format = request.data.get('result_format', 'json')
        result = custom_fizz_bizz(generator, amount_of_result, offset)
        return response.Response(get_result_by_format(result, format))
    return response.Response("You don't have enough permission!", status=status.HTTP_400_BAD_REQUEST)
