from django.contrib import admin
from .models import Generator, Divisor


@admin.register(Generator)
class GeneratorAdmin(admin.ModelAdmin):
    exclude = ['url']
    list_display = ['name', 'url']


@admin.register(Divisor)
class DivisorAdmin(admin.ModelAdmin):
    list_display = ['string', 'number']
