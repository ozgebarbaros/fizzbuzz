## FizzBuzz WebService ##

## Requirements ##
* python 3.6
* django 2.1

## Installation ## 

First create virtualenv:

````bash
python3.6 -m venv
````
````bash
source venv/bin/activate
````
Install requirements:
````bash
pip install -r requirements.txt
````

Create migrations:
````bash
python manage.py makemigrations
````

Apply migrations:
````bash
python manage.py migrate
````
## Loading Data (You don't need this if you will run unit tests.)##
If you want to work with pre-created data you can easily load the fixtures with following commands
````bash
python manage.py loaddata test_fixtures.json
````
## Testing ##

You can run test cases with following command:

````bash
python manage.py test
````
If you want to run tests with your adjusted data and adjusted test, you must first dump db to test fixture with following command:

````bash
python manage.py dumpdata > fixtures/test_fixtures.json
````

And then may run tests

````bash
python manage.py test
````



